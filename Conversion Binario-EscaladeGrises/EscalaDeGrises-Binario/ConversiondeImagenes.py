# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Lectura de Imagen con Opencv
# Descripción: Visualizar imagen desde un directorio externo
# Conversion de Imagenes RGB a Escala de Grises y de Escala de Grises a Binarias

#importamos Libreria
import cv2
#Leer imagen con Libreria con OpenCV
imagen =cv2.imread('../Externo/puntos.png')
#Lectura de imagen y transformacion a escala de grises
#un punto accede hace dentro
#im_gray = cv2.imread('./imagenes/lena.png', cv2.IMREAD_GRAYSCALE)
#dos puntos accede aun directorio externo fuera
im_gray = cv2.imread('../Externo/puntos.png', cv2.IMREAD_GRAYSCALE)
#Lectura de Imagen Escala de Grises  y transformacion a Binaria
t,dst= cv2.threshold(im_gray, 170,255, cv2.THRESH_BINARY)

#Mostrar Imagen RGB
cv2.imshow('RGB_IMAGEN', imagen)
#Mostrar Imagen Blanco y Negro
cv2.imshow('Blanco y Negro',im_gray)
#Guardar Imagen Resultante Gray
cv2.imwrite('./Salida/Puntos_Gray.png', im_gray)
#Mostrar Imagen Binaria
cv2.imshow('Binario',dst)
#Guardar Imagen Resultante Binario
cv2.imwrite('./Salida/Puntos_Binario.png',dst)

#Espera presionar una tecla para destruir una ventana
cv2.waitKey(0)

#Cierra las ventanas
cv2.destroyAllWindows()