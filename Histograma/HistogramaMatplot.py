# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Histograma de Imagenes usando Matplotlib
# Descripción: Visualizar de Histograma de Imagenes Escala de Grises

# Importar Libreria OpenCV
import cv2 as cv
# Importar Libreria Matplotlib
from matplotlib import pyplot as plt

#Leer imagen con Libreria con OpenCV
img = cv.imread('./imagenes/imagen4.png')

# Histograma de Imagen
color = ('b','g','r')
for i,col in enumerate(color):
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.plot(histr,color = col)
    plt.xlim([0,256])
plt.show()