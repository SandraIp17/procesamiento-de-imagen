# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Histograma de Imagenes con OpenCV
# Descripción: Visualizar de Imagen Original y Ecualizada en conjunto con su respectivo Histograma

# Importar Libreria OpenCV
import cv2 as cv
# Importar Libreria Matplotlib
from matplotlib import pyplot as plt

#Leer imagen con Libreria con OpenCV
img= cv.imread('./imagenes/imagen_1.png',0)
resul=cv.imread('./imagenes/resultimagen_1.jpg',0)


# Calculate histogram with mask and without mask
# Check third argument for mask
hist_full = cv.calcHist([img],[0],None,[256],[0,256])
hist_resul = cv.calcHist([resul],[0],None,[256],[0,256])
plt.subplot(221), plt.imshow(img, 'gray')
plt.subplot(223), plt.imshow(resul,'gray')
plt.subplot(222), plt.plot(hist_full )
plt.subplot(224), plt.plot(hist_resul)
plt.xlim([0,256])
plt.show()