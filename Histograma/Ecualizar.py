# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Ecualizar Imagen
# Descripción: Implementa la ecualizacion de una imagen

# Importar Libreria OpenCV
import cv2 as cv

#Leer Imagen con Libreria OpenCV
img = cv.imread('./imagenes/imagen4.png')
# Ecualizar Imagen
img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)
#Guardar Imagen Ecualizada
cv.imwrite('./imagenes/resultimagen4.jpg', hist_equalization_result)
