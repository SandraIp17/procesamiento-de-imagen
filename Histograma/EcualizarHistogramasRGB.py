# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Ecualizacion e Histograma de Imagenes
# Descripción: Visualizar de Imagen Original y Ecualizada en conjunto con su respectivo Histograma
# Conversion de Imagenes BGR a RGB

# Importar Libreria OpenCV
import cv2 as cv
# Importar Libreria Numpy
import numpy as np
# Importar Libreria Matplotlib
from matplotlib import pyplot as plt

#Leer imagen con Libreria con OpenCV
img = cv.imread('./imagenes/imagen4.png')
#Comvertir imagen BGR  a RGB
b,g,r = cv.split(img)       # get b,g,r
rgb_img = cv.merge([r,g,b])
#Ecualizar Imagen
img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)
#Guardar Imagen Ecualizada
cv.imwrite('./imagenes/resultimagen4.jpg', hist_equalization_result)
#Leer Imagen Ecualizada
resul=cv.imread('./imagenes/resultimagen4.jpg')
#Convertir Imagen BGR a RGB
b,g,r = cv.split(resul)
rgb_resul = cv.merge([r,g,b])

#Mostrar Imagen RGB Original
plt.subplot(221), plt.imshow(rgb_img, 'gray')
#Mostrar Imagen RGB Ecualizada
plt.subplot(223), plt.imshow(rgb_resul,'gray')
#Mostrar Histograma
color = ('b','g','r')
for i,col in enumerate(color):
    #Histograma de Imagen Original
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.subplot(222),plt.plot(histr,color = col)
    # Histogrma de Imagen Ecualizada
    histrecualizada = cv.calcHist([resul], [i], None, [256], [0, 256])
    plt.subplot(224), plt.plot(histrecualizada, color=col)

plt.xlim([0,256])
plt.show()