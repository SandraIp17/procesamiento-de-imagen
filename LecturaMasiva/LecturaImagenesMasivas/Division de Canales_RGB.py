# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Descripcion: Extraccion de Canales de varias imagenes

# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os

initSequence=1 # inicio secuencia
numSequences= 100 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_RGB = "../DATASETS/17flowers/RGB"


# Directorios de escritura
# Imágenes GRAY
path_GRAY = "./Prueba"
# Imágenes BINARIAS (B/N)
path_Binary = "./Output-Binary"

# Canal R (RED)
path_R = "./channel-RED"
# Canal G (GREEN)
path_G = "./channel-GREEN"
# Canal B (BLUE)
path_B = "./channel-BLUE"

totalImages = int(len(os.listdir(path_RGB))) # Contabiliza número de archivos
# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_RGB + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)
    # Transformar a ESCALA DE GRISES la imagen en color
    gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    #Transformar a Binario la imagen en escala de grises
    t, dst = cv.threshold(gray, 170, 255, cv.THRESH_BINARY)
    #Extraccion de Canales RGB
    B = cv.extractChannel(img, 0)
    G = cv.extractChannel(img, 1)
    R = cv.extractChannel(img, 2)
    #Guardar imagenes Escala de Grises
    cv.imwrite(path_GRAY + '/pimage_' + str(ns).zfill(4) + '.jpg', gray)
    # Guardar imagenes Binarias
    cv.imwrite(path_Binary + '/binaryimage_' + str(ns).zfill(4) + '.jpg', dst)
    # Guardar imagenes en el canal BlUE
    cv.imwrite(path_B + '/Bimage_' + str(ns).zfill(4) + '.jpg', B)
    # Guardar imagenes en el canal RED
    cv.imwrite(path_R + '/RBimage_' + str(ns).zfill(4) + '.jpg', R)
    # Guardar imagenes en el canal GREEN
    cv.imwrite(path_G + '/Gimage_' + str(ns).zfill(4) + '.jpg', G)

    print(dirimages)
    print(cont_frame)
    print(totalImages)