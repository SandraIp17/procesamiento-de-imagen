# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Descripcion: Lectura de varias imagenes que seran guardadas en un nuevo directorio

# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os

initSequence=1 # inicio secuencia
numSequences= 100 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_RGB = "../DATASETS/17flowers/RGB"


# Directorios de escritura
# Imágenes GRAY
path_GRAY = "./Prueba"
# Imágenes BINARIAS (B/N)
path_Binary = "./Output-Binary"

# Contabiliza número de archivos
totalImages = int(len(os.listdir(path_RGB)))
# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_RGB + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)
    # Transformar a ESCALA DE GRISES la imagen RGB
    gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    #Transformar a Binario la imagen en escala de grises
    t, dst = cv.threshold(gray, 170, 255, cv.THRESH_BINARY)
    cv.imshow('RGB Image', img_rgb)
    #Guardamos las imagenes a Escala de Grises
    cv.imwrite(path_GRAY + '/pimage_' + str(ns).zfill(4) + '.jpg', gray)
    #Guardamos las imagenes Binarias
    cv.imwrite(path_Binary + '/binaryimage_' + str(ns).zfill(4) + '.jpg', dst)


    print(dirimages)
    print(cont_frame)