# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Operacion Logicas en OpenCV con Imagenes
# Descripción: Visualizar imagen con los operadores logicos OR, AND, NOT, XOR


#Importar Libreria OpenCV
import cv2 as cv

# Leer imagen Mitad del Mundo
img1=cv.imread('./imagenes/cuadrado.png')
# Leer imagen Torre Eiffel
img2=cv.imread('./imagenes/Elipse.png')

# Despliega dimensiones de imagenes
print(img1.shape)
print(img2.shape)


# Realizar operaciones Logicas
#Operador Logico OR
res=cv.bitwise_or(img1,img2)
#Operador Logico AND
#res=cv.bitwise_and(img1,img2)
#Operador Logico NOT
#res=cv.bitwise_not(img1,img2)
#Operador Logico XOR
#res=cv.bitwise_xor(img1,img2,None,None)

cv.imwrite('./imagenes/ResultAND.jpg', res)
#Mostramos imagenes
cv.imshow('Imagen 1',img1)
cv.imshow('Imagen 2',img2)
cv.imshow('Resultado',res)

cv.waitKey(0)
cv.destroyAllWindows()