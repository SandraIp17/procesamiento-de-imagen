# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Uso de OpenCV para Adición de imágenes
# Descripción: Visualizar de adicion de imagenes Binarias

# Importar Libreria OpenCV
import cv2 as cv
#
# Leer imagen Cuadrado
img1=cv.imread('./imagenes/cuadrado.png')
# Leer imagen Elipse
img2=cv.imread('./imagenes/Elipse.png')

# Despliega dimensiones de imagenes
print(img1.shape)
print(img2.shape)

# Adicion de Imagenes
suma= cv.add(img1,img2)
# Mezclar Imagenes con porcentajes
#suma= cv.addWeighted(img1,0.8,img2,0.2,0.5)
cv.imwrite('./imagenes/ResultSumaFiguras.jpg', suma)

#Mostrar Imagen 1
cv.imshow('Imagen 1',img1)
#Mostrar Imagen 1
cv.imshow('Imagen 2',img2)
#Mostrar Imagen Resultante
cv.imshow('Suma de Imagenes',suma)

cv.waitKey(0)
cv.destroyAllWindows()
