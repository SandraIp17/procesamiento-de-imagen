# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Uso de OpenCV para Sustraccion de imágenes
# Descripción: Visualizar de sustraccion de imagenes

# Importar Libreria OpenCV
import cv2 as cv

# Leer imagen Mitad del Mundo
img1=cv.imread('./imagenes/mitad_mundo.jpg')
# Leer imagen Torre Eiffel
img2=cv.imread('./imagenes/torre_eiffel.jpg')
# Despliega dimensiones de imagenes
print(img1.shape)
print(img2.shape)

#Redimensiona una imagen
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)

#Suma de Imagenes
#suma= cv.add(imagen1_res,img2)
#Adicion de Imagenes con porcentajes
suma= cv.addWeighted(imagen1_res,0.8,img2,0.2,0.5)
#Guardar Imagen Resultante
cv.imwrite('./imagenes/ResultSumaPorcentaje.jpg', suma)
#Mostramos imagenes
cv.imshow('Imagen 1',img1)
cv.imshow('Imagen 2',img2)
cv.imshow('Suma de Imagenes',suma)


cv.waitKey(0)
cv.destroyAllWindows()
