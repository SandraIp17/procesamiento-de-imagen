# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Uso de OpenCV para Sustraccion de imágenes
# Descripción: Visualizar de sustraccion de imagenes

# Importar libreria OpenCV
import cv2 as cv

# Leer imagen Cuadrado
img1=cv.imread('./imagenes/cuadrado.png')
# Leer imagen Torre Eiffel
img2=cv.imread('./imagenes/Elipse.png')
# Realizar operaciones solicitadas

# Despliega dimensiones de imagenes
print(img1.shape)
print(img2.shape)

#Sustracion de Imagenes por porcentajes
resta= cv.absdiff(img1,img2)
#resta2= cv.subtract(img1,img2)
#Guardar Imagen Resultante
cv.imwrite('./imagenes/ResultRest.jpg', resta)
#Mostramos imagenes
cv.imshow('Imagen 1',img1)
cv.imshow('Imagen 2',img2)
cv.imshow('Resta de Imagenes',resta)

cv.waitKey(0)
cv.destroyAllWindows()